package junitParallel;

// when we are  using SuiteClass of Junit it First execute the all the testCases of TestClassA . 
//After that all the testCases of TestClassB. To overcome of this problem we use ParallelComputer class and JunitCore of Junit.
	
	import org.junit.runner.RunWith;
	import org.junit.runners.Suite;
	import org.junit.runners.Suite.SuiteClasses;

	@RunWith(Suite.class)
	@SuiteClasses({ TestClassA.class, TestClassB.class })

	public class TestSuit {
}
